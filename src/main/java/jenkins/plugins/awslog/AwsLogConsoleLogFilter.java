package jenkins.plugins.awslog;

import hudson.console.ConsoleLogFilter;
import hudson.model.AbstractBuild;
import hudson.model.AbstractProject;
import hudson.model.Run;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author fye
 * @email fh941005@163.com
 * @date 2020-03-21 00:21
 * @description
 */
public class AwsLogConsoleLogFilter extends ConsoleLogFilter implements Serializable {
    private static final Logger LOGGER = Logger.getLogger(AwsLogConsoleLogFilter.class.getName());

    private transient Run<?, ?> run;

    public AwsLogConsoleLogFilter() {
    }

    public AwsLogConsoleLogFilter(Run<?, ?> run) {
        this.run = run;
    }

    private static final long serialVersionUID = 1L;

    @Override
    public OutputStream decorateLogger(Run build, OutputStream logger) throws IOException, InterruptedException {
        AwsLogConfiguration configuration = AwsLogConfiguration.getInstance();
        if (!configuration.isEnabled()) {
            LOGGER.log(Level.FINE, "this plugin is disabled. Logs will not be forwarded.");
            return logger;
        }

        if (build instanceof AbstractBuild<?, ?>) {
            if (isAwsLogEnabled(build)) {
                AwsLogWriter awsLogWriter = getAwsLogWriter(build, logger);
                return new AwsLogOutputStream(logger, awsLogWriter);
            } else {
                return logger;
            }
        }
        if (run != null) {
            AwsLogWriter logWriter = getAwsLogWriter(run, logger);
            return new AwsLogOutputStream(logger, logWriter);
        } else {
            return logger;
        }
    }

    AwsLogWriter getAwsLogWriter(Run<?, ?> build, OutputStream errorStream) {
        return new AwsLogWriter(build, errorStream, null, build.getCharset());
    }

    private boolean isAwsLogEnabled(Run<?, ?> build) {
        AwsLogConfiguration configuration = AwsLogConfiguration.getInstance();
        if (configuration.isEnableGlobally()) {
            return true;
        }

        if (build.getParent() instanceof AbstractProject) {
            AbstractProject<?, ?> project = (AbstractProject<?, ?>) build.getParent();
            return project.getProperty(AwsLogJobProperty.class) != null;
        }
        return false;
    }
}
