package jenkins.plugins.awslog;

import hudson.console.ConsoleNote;
import hudson.console.LineTransformationOutputStream;

import java.io.IOException;
import java.io.OutputStream;

/**
 * @author fye
 * @email fh941005@163.com
 * @date 2020-03-21 00:32
 * @description
 */
public class AwsLogOutputStream extends LineTransformationOutputStream {
    private final OutputStream delegate;
    private final AwsLogWriter awsLogWriter;

    public AwsLogOutputStream(OutputStream delegate, AwsLogWriter awsLogWriter) {
        super();
        this.delegate = delegate;
        this.awsLogWriter = awsLogWriter;
    }


    @Override
    protected void eol(byte[] b, int len) throws IOException {
        delegate.write(b, 0, len);
        this.flush();

        if (!awsLogWriter.isConnectionBroken()) {
            String line = new String(b, 0, len, awsLogWriter.getCharset());
            line = ConsoleNote.removeNotes(line).trim();
            awsLogWriter.write(line);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void flush() throws IOException {
        delegate.flush();
        super.flush();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close() throws IOException {
        delegate.close();
        super.close();
    }
}
