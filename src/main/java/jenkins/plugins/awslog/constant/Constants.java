package jenkins.plugins.awslog.constant;

/**
 * @author fye
 * @email fh941005@163.com
 * @date 2020-03-20 23:57
 * @description
 */
public class Constants {
    /**
     * protocol enum for user choose
     */
    public enum Protocol {
        http,
        https
    }
}
