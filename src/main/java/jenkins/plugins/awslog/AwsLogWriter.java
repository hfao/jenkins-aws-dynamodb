package jenkins.plugins.awslog;

import hudson.model.AbstractBuild;
import hudson.model.Run;
import hudson.model.TaskListener;
import jenkins.model.Jenkins;
import jenkins.plugins.awslog.kit.HttpKit;
import jenkins.plugins.awslog.persistence.BuildData;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.time.FastDateFormat;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.*;

/**
 * @author fye
 * @email fh941005@163.com
 * @date 2020-03-21 00:25
 * @description
 */
public class AwsLogWriter {
    private final OutputStream errorStream;
    private final Run<?, ?> build;
    private final TaskListener listener;
    private final BuildData buildData;
    private boolean connectionBroken;
    private final Charset charset;
    private final AwsLogConfiguration configuration;

    public AwsLogWriter(Run<?, ?> run, OutputStream error, TaskListener listener, Charset charset) {
        this.errorStream = error != null ? error : System.err;
        this.build = run;
        this.listener = listener;
        this.charset = charset;
        this.buildData = getBuildData();
        this.configuration = AwsLogConfiguration.getInstance();
    }

    /**
     * Gets the charset that Jenkins is using during this build.
     *
     * @return the charset
     */
    public Charset getCharset() {
        return charset;
    }

    /**
     * Sends a logstash payload for a single line to the indexer.
     * Call will be ignored if the line is empty or if the connection to the indexer is broken.
     * If write fails, errors will logged to errorStream and connectionBroken will be set to true.
     *
     * @param line Message, not null
     */
    public void write(String line) {
        if (!isConnectionBroken() && StringUtils.isNotEmpty(line)) {
            this.write(Collections.singletonList(line));
        }
    }

    /**
     * Sends a logstash payload containing log lines from the current build.
     * Call will be ignored if the connection to the indexer is broken.
     * If write fails, errors will logged to errorStream and connectionBroken will be set to true.
     *
     * @param maxLines Maximum number of lines to be written.  Negative numbers mean "all lines".
     */
    public void writeBuildLog(int maxLines) {
        if (!isConnectionBroken()) {
            // FIXME: build.getLog() won't have the last few lines like "Finished: SUCCESS" because this hasn't returned yet...
            List<String> logLines;
            try {
                if (maxLines < 0) {
                    logLines = build.getLog(Integer.MAX_VALUE);
                } else {
                    logLines = build.getLog(maxLines);
                }
            } catch (IOException e) {
                String msg = "[AwsLog-plugin]: Unable to serialize log data.\n" +
                        ExceptionUtils.getStackTrace(e);
                logErrorMessage(msg);

                // Continue with error info as logstash payload
                logLines = Arrays.asList(msg.split("\n"));
            }

            for (String line : logLines) {
                System.out.println(System.currentTimeMillis() + "：" + line);
            }
            write(logLines);
        }
    }

    /**
     * @return True if errors have occurred during initialization or write.
     */
    public boolean isConnectionBroken() {
        return connectionBroken || build == null || buildData == null;
    }

    BuildData getBuildData() {
        if (build instanceof AbstractBuild) {
            return new BuildData((AbstractBuild<?, ?>) build, new Date(), listener);
        } else {
            return new BuildData(build, new Date(), listener);
        }
    }

    private JSONObject buildPayload(BuildData buildData, String jenkinsUrl, List<String> logLines) {
        JSONObject payload = new JSONObject();
        payload.put("data", buildData.toJson());
        payload.put("message", logLines);
        payload.put("source", "jenkins");
        payload.put("sourceHost", jenkinsUrl);
        payload.put("buildTimestamp", buildData.getTimestamp());
        payload.put("timestamp", FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss")
                .format(Calendar.getInstance().getTime()));
        return payload;
    }

    /**
     * Write a list of lines to the aws as one payload.
     */
    private void write(List<String> lines) {
        if (StringUtils.isEmpty(this.configuration.getUrl())) {
            logErrorMessage("The server url is required.");
            return;
        }
        buildData.updateResult();
        JSONObject payload = buildPayload(buildData, Jenkins.get().getRootUrl(), lines);
        Map<String, String> reqMap = new HashMap<>(1);
        reqMap.put("payload", payload.toString());
        HttpKit.post(this.configuration.getUrl(), reqMap, null);
    }

    /**
     * Write error message to errorStream and set connectionBroken to true.
     */
    private void logErrorMessage(String msg) {
        try {
            connectionBroken = true;
            errorStream.write(msg.getBytes(charset));
            errorStream.flush();
        } catch (IOException ex) {
            // This should never happen, but if it does we just have to let it go.
            ex.printStackTrace();
        }
    }

}
