package jenkins.plugins.awslog;

import hudson.Extension;
import hudson.tools.ToolDescriptor;
import hudson.tools.ToolInstallation;
import hudson.tools.ToolProperty;
import jenkins.model.Jenkins;
import jenkins.plugins.awslog.constant.Constants;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.DataBoundSetter;

import java.util.List;

/**
 * @author fye
 * @email fh941005@163.com
 * @date 2020-03-20 23:53
 * @description
 */
public class AwsLogInstallation extends ToolInstallation {
    private static final long serialVersionUID = -5730780734005293851L;

    @DataBoundConstructor
    public AwsLogInstallation(String name, String home, List<? extends ToolProperty<?>> properties) {
        super(name, home, properties);
    }

    public static Descriptor getAwsLogDescriptor() {
        return (Descriptor) Jenkins.get().getDescriptor(AwsLogInstallation.class);
    }

    @Extension
    public static final class Descriptor extends ToolDescriptor<AwsLogInstallation> {
        private transient Constants.Protocol protocol;
        private transient String host;
        private transient int port = -1;
        private transient String method;

        public Descriptor() {
            super();
            load();
        }


        @Override
        public String getDisplayName() {
            return "AwsLogCollector";
        }

        public Constants.Protocol getProtocol() {
            return protocol;
        }

        @DataBoundSetter
        public void setProtocol(Constants.Protocol protocol) {
            this.protocol = protocol;
        }

        public String getHost() {
            return host;
        }

        @DataBoundSetter
        public void setHost(String host) {
            this.host = host;
        }

        public int getPort() {
            return port;
        }

        @DataBoundSetter
        public void setPort(int port) {
            this.port = port;
        }

        public String getMethod() {
            return method;
        }

        @DataBoundSetter
        public void setMethod(String method) {
            this.method = method;
        }
    }
}
