package jenkins.plugins.awslog.kit;

import java.util.regex.Pattern;

/**
 * @author fye
 * @email fh941005@163.com
 * @date 2020-03-20 23:45
 * @description
 */
public class RegKit {
    private static final String REGEX_HOST = "^([a-zA-Z\\d][a-zA-Z\\d-_]+\\.)+[a-zA-Z\\d-_][^ ]*$";
    private static final String REGEX_IP = "^(\\\\d|[1-9]\\\\d|1\\\\d*|2[0-4]\\\\d|25[0-5])(\\\\.\\\\1){3}$";

    public static boolean isHost(String val) {
        return Pattern.matches(REGEX_HOST, val);
    }

    public static boolean isIp(String val) {
        return Pattern.matches(REGEX_IP, val);
    }
}
