package jenkins.plugins.awslog;

import hudson.Extension;
import hudson.model.AbstractProject;
import hudson.model.Job;
import hudson.model.JobProperty;
import hudson.model.JobPropertyDescriptor;
import net.sf.json.JSONObject;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.StaplerRequest;

/**
 * @author fye
 * @email fh941005@163.com
 * @date 2020-03-21 00:35
 * @description
 */
public class AwsLogJobProperty extends JobProperty<Job<?, ?>> {
    @DataBoundConstructor
    public AwsLogJobProperty() {
    }

    @Extension
    public static class DescriptorImpl extends JobPropertyDescriptor {
        @Override
        public JobProperty<?> newInstance(StaplerRequest req, JSONObject formData) throws FormException {
            if (formData.containsKey("enable")) {
                return new AwsLogJobProperty();
            }
            return null;
        }

        @Override
        public String getDisplayName() {
            return "AwsLogCollector";
        }

        @Override
        public boolean isApplicable(Class<? extends Job> jobType) {
            return AbstractProject.class.isAssignableFrom(jobType);
        }
    }
}
