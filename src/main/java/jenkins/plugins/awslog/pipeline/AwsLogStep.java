package jenkins.plugins.awslog.pipeline;

import hudson.Extension;
import hudson.console.ConsoleLogFilter;
import hudson.model.Run;
import jenkins.YesNoMaybe;
import jenkins.plugins.awslog.AwsLogConsoleLogFilter;
import org.jenkinsci.plugins.workflow.steps.*;
import org.kohsuke.stapler.DataBoundConstructor;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * @author fye
 * @email fh941005@163.com
 * @date 2020-03-21 00:19
 * @description
 */
public class AwsLogStep extends Step {
    @DataBoundConstructor
    public AwsLogStep() {
    }

    @Override
    public StepExecution start(StepContext context) throws Exception {
        return new AwsLogStep.Execution(context);
    }

    /**
     * Execution for {@link AwsLogStep}.
     */
    public static class Execution extends AbstractStepExecutionImpl {

        public Execution(StepContext context) {
            super(context);
        }

        private static final long serialVersionUID = 1L;

        @Override
        public void onResume() {
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean start() throws Exception {
            StepContext context = getContext();
            context.newBodyInvoker()
                    .withContext(createConsoleLogFilter(context))
                    .withCallback(BodyExecutionCallback.wrap(context))
                    .start();
            return false;
        }

        private ConsoleLogFilter createConsoleLogFilter(StepContext context)
                throws IOException, InterruptedException {
            ConsoleLogFilter original = context.get(ConsoleLogFilter.class);
            Run<?, ?> build = context.get(Run.class);
            ConsoleLogFilter subsequent = new AwsLogConsoleLogFilter(build);
            return BodyInvoker.mergeConsoleLogFilters(original, subsequent);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void stop(@Nonnull Throwable cause) throws Exception {
            getContext().onFailure(cause);
        }
    }

    /**
     * Descriptor for {@link LogstashStep}.
     */
    @Extension(dynamicLoadable = YesNoMaybe.YES, optional = true)
    public static class DescriptorImpl extends StepDescriptor {

        /**
         * {@inheritDoc}
         */
        @Override
        public String getDisplayName() {
            return "Send individual log lines to Aws";
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getFunctionName() {
            return "AwsLog";
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean takesImplicitBlockArgument() {
            return true;
        }

        @Override
        public Set<? extends Class<?>> getRequiredContext() {
            Set<Class<?>> contexts = new HashSet<>();
            contexts.add(Run.class);
            return contexts;
        }
    }
}
