package jenkins.plugins.awslog;

import hudson.Extension;
import hudson.init.InitMilestone;
import hudson.init.Initializer;
import hudson.util.FormValidation;
import jenkins.model.GlobalConfiguration;
import jenkins.plugins.awslog.constant.Constants;
import jenkins.plugins.awslog.kit.HttpKit;
import jenkins.plugins.awslog.kit.RegKit;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.kohsuke.stapler.DataBoundSetter;
import org.kohsuke.stapler.QueryParameter;
import org.kohsuke.stapler.StaplerRequest;

import java.util.logging.Logger;

/**
 * @author fye
 * @email fh941005@163.com
 * @date 2020-03-20 23:31
 * @description rewrite for all
 */
@Extension
public class AwsLogConfiguration extends GlobalConfiguration {
    private static final Logger LOGGER = Logger.getLogger(AwsLogConfiguration.class.getName());
    /**
     * enable to use this plugin
     */
    private Boolean enabled;
    /**
     * global use or not
     */
    private boolean enableGlobally = false;
    /**
     * aws logger collector server info
     */
    private Constants.Protocol protocol;
    private String host;
    private int port;
    private String method;

    /**
     * final request url
     */
    private String url;

    /**
     * flag of configured or not
     */
    private transient boolean configuring = false;
    private boolean dataMigrated = false;

    public AwsLogConfiguration() {
        load();
        if (enabled == null) {
            enabled = StringUtils.isEmpty(host);
        }
    }

    @Initializer(after = InitMilestone.JOB_LOADED)
    public void migrateData() {
        this.url = this.protocol + "://" + this.host + ":"
                + this.port + this.method;
        save();
    }

    @Override
    public boolean configure(StaplerRequest staplerRequest, JSONObject json) throws FormException {
        // When not enabling the plugin we just save the enabled state
        // without binding the JSON and then return. This avoids problems with missing configuration
        // like URLs which can't be parsed when empty, which would lead to errors in the UI.
        boolean e = json.getBoolean("enabled");
        if (!e) {
            enabled = false;
            save();
            return true;
        }
        configuring = true;
        staplerRequest.bindJSON(this, json);
        save();
        return true;
    }

    /**
     * check host info
     *
     * @param value
     * @return
     */
    public FormValidation doCheckHost(@QueryParameter String value) {
        if (value.isEmpty()) {
            return FormValidation.error("You must provide an Hostname or IpAddress.");
        }
        if (RegKit.isHost(value) || RegKit.isIp(value)) {
            return FormValidation.ok();
        }
        return FormValidation.error("This is not a valid Hostname or IpAddress.");
    }

    public FormValidation doCheckPort(@QueryParameter String value) {
        if (value.isEmpty()) {
            return FormValidation.error("You must provide an port like 8090");
        }
        try {
            int val = Integer.parseInt(value);
            if (val >= 1 && val <= 65535) {
                return FormValidation.ok();
            }
            return FormValidation.error("you must provide an integer value between 1 and 65535");
        } catch (Exception e) {
            return FormValidation.error("you must provide an integer value between 1 and 65535");
        }
    }

    /**
     * combine url and test it
     *
     * @param protocol
     * @param host
     * @param port
     * @return
     */
    public FormValidation doTestURL(@QueryParameter("protocol") String protocol,
                                    @QueryParameter("host") String host,
                                    @QueryParameter("port") String port,
                                    @QueryParameter("method") String method) {
        String url = protocol + "://" + host + ":" + port + method;
        try {
            String res = HttpKit.get(url);
            if ("success".equals(res)) {
                this.url = url;
                return FormValidation.ok("success");
            } else {
                this.url = "";
                return FormValidation.error("error");
            }
        } catch (Exception e) {
            this.url = "";
            return FormValidation.error(e.getLocalizedMessage());
        }
    }

    public boolean isEnabled() {
        return enabled;
    }

    @DataBoundSetter
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isEnableGlobally() {
        return enableGlobally;
    }

    @DataBoundSetter
    public void setEnableGlobally(boolean enableGlobally) {
        this.enableGlobally = enableGlobally;
    }

    public Constants.Protocol getProtocol() {
        return protocol;
    }

    @DataBoundSetter
    public void setProtocol(Constants.Protocol protocol) {
        this.protocol = protocol;
    }

    public String getHost() {
        return host;
    }

    @DataBoundSetter
    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    @DataBoundSetter
    public void setPort(int port) {
        this.port = port;
    }

    public String getMethod() {
        return method;
    }

    @DataBoundSetter
    public void setMethod(String method) {
        this.method = method;
    }

    public static AwsLogConfiguration getInstance() {
        return GlobalConfiguration.all().get(AwsLogConfiguration.class);
    }

    public String getUrl() {
        return url;
    }
}
