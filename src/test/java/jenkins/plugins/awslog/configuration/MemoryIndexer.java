package jenkins.plugins.awslog.configuration;

import jenkins.plugins.awslog.persistence.MemoryDao;

public class MemoryIndexer extends LogstashIndexer<MemoryDao>
{
  final MemoryDao dao;

  public MemoryIndexer(MemoryDao dao)
  {
    this.dao = dao;
  }

  @Override
  protected MemoryDao createIndexerInstance()
  {
    return dao;
  }

}
