Description
=
I am not be familiar with Jenkins plugin development, so i make changes based on the existing plugins of "logstash-plugin"

Detail Procedures
=
* Run the plugin use "mvn hpi:run -Djetty.port=7080"
* Visit http://localhost:7080/jenkins/configure to config Global info, like this:

    ![](./images/1.png)
    
    You can choose the Protocol(http/https), write hostname, port, and method name for server to receive the data. 
    Click the test button can test the server info is effective or not, if error will show the error info.
    I deploy the application on my server, and the hostname is: jenkins.api.fabrice.cn
* Create a pipeline project

    ![](./images/2.png)  
    
* Script like this:
    ```
    AwsLog{
        node("master"){
        greet 'fenghao'
        greet 'fenghao'
        greet 'fenghao'
        }
    }
    ```  
    AwsLog is the plugin function name, and greet is the example what I modified based on HelloWord Plugin, which will output the name 10 times cycled.  
    ![](./images/3.png)    
* Click '立即构建' 5 times at the same time